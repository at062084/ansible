## [3.4.1](https://gitlab.com/to-be-continuous/ansible/compare/3.4.0...3.4.1) (2022-12-17)


### Bug Fixes

* hanging awk script ([e4d10f8](https://gitlab.com/to-be-continuous/ansible/commit/e4d10f80377f4fc1e67f00d19b12485a5c7560b6))

# [3.4.0](https://gitlab.com/to-be-continuous/ansible/compare/3.3.0...3.4.0) (2022-12-16)


### Features

* standardize env url management ([b41057c](https://gitlab.com/to-be-continuous/ansible/commit/b41057c5c2e0987486a03ec281c87d88a84fa724))

# [3.3.0](https://gitlab.com/to-be-continuous/ansible/compare/3.2.0...3.3.0) (2022-12-13)


### Features

* **vault:** configurable Vault Secrets Provider image ([2a12d43](https://gitlab.com/to-be-continuous/ansible/commit/2a12d4327f491387528abadc350fc142b3daaa46))

# [3.2.0](https://gitlab.com/to-be-continuous/ansible/compare/3.1.0...3.2.0) (2022-10-10)


### Features

* use gitlab-ci references to make deploy and cleaup jobs easier to modify ([03cac3c](https://gitlab.com/to-be-continuous/ansible/commit/03cac3c802062064d04e3bfc8cbdc632c66259cf))

# [3.1.0](https://gitlab.com/to-be-continuous/ansible/compare/3.0.0...3.1.0) (2022-10-04)


### Features

* normalize reports ([6ff3fbc](https://gitlab.com/to-be-continuous/ansible/commit/6ff3fbc5f921a6404def6e6319326f332cb1f8c5))

# [3.0.0](https://gitlab.com/to-be-continuous/ansible/compare/2.3.0...3.0.0) (2022-08-05)


### Features

* adaptive pipeline ([f04a9ed](https://gitlab.com/to-be-continuous/ansible/commit/f04a9edbeb7e752b986fcbf7ca44c9c96ec8881d))


### BREAKING CHANGES

* change default workflow from Branch pipeline to MR pipeline

# [2.3.0](https://gitlab.com/to-be-continuous/ansible/compare/2.2.0...2.3.0) (2022-06-30)


### Features

* enforce AUTODEPLOY_TO_PROD and PUBLISH_ON_PROD as boolean variables ([6d3fab7](https://gitlab.com/to-be-continuous/ansible/commit/6d3fab75e8ca41392213c9d66e2a98f5ec8d25d9))

# [2.2.0](https://gitlab.com/to-be-continuous/ansible/compare/2.1.4...2.2.0) (2022-05-01)


### Features

* configurable tracking image ([0eb30e0](https://gitlab.com/to-be-continuous/ansible/commit/0eb30e0347c3a4d80f8dfe9df2e65f3e72ed341f))

## [2.1.4](https://gitlab.com/to-be-continuous/ansible/compare/2.1.3...2.1.4) (2022-02-24)


### Bug Fixes

* **vault:** revert Vault JWT authentication not working ([05d0a7e](https://gitlab.com/to-be-continuous/ansible/commit/05d0a7ec99d6127a8b891937cb5e1be410ca871c))

## [2.1.3](https://gitlab.com/to-be-continuous/ansible/compare/2.1.2...2.1.3) (2022-02-23)


### Bug Fixes

* **vault:** Vault JWT authentication not working ([1c8fe18](https://gitlab.com/to-be-continuous/ansible/commit/1c8fe187725f8c95e2eec5c5cd6ca6c101223baf))

## [2.1.2](https://gitlab.com/to-be-continuous/ansible/compare/2.1.1...2.1.2) (2022-01-10)


### Bug Fixes

* non-blocking warning in case failed decoding [@url](https://gitlab.com/url)@ variable ([0ccef58](https://gitlab.com/to-be-continuous/ansible/commit/0ccef58f69958d7ce20e692f5b9f10017a88c7ee))

## [2.1.1](https://gitlab.com/to-be-continuous/ansible/compare/2.1.0...2.1.1) (2021-12-03)


### Bug Fixes

* execute hook scripts with shebang shell ([386ec1d](https://gitlab.com/to-be-continuous/ansible/commit/386ec1d72b5d48d4be150133c6c892aa196f122f))

# [2.1.0](https://gitlab.com/to-be-continuous/ansible/compare/2.0.1...2.1.0) (2021-10-12)


### Features

* manage static or dynamic environment urls ([fd82064](https://gitlab.com/to-be-continuous/ansible/commit/fd82064c3cd3759ee1db283e6329eb44cce294ae))

## [2.0.1](https://gitlab.com/to-be-continuous/ansible/compare/2.0.0...2.0.1) (2021-10-07)


### Bug Fixes

* use master or main for production env ([63479be](https://gitlab.com/to-be-continuous/ansible/commit/63479bed3353ea1d65794dd8e33356427f7b9712))

## [2.0.0](https://gitlab.com/to-be-continuous/ansible/compare/1.2.1...2.0.0) (2021-09-02)

### Features

* Change boolean variable behaviour ([f24358b](https://gitlab.com/to-be-continuous/ansible/commit/f24358b211a2d46a04ef76909f255e027c3afcdc))

### BREAKING CHANGES

* boolean variable now triggered on explicit 'true' value

## [1.2.1](https://gitlab.com/to-be-continuous/ansible/compare/1.2.0...1.2.1) (2021-07-08)

### Bug Fixes

* conflict between vault and scoped vars ([689fc86](https://gitlab.com/to-be-continuous/ansible/commit/689fc86cdeebbe0a752f792d238acadcb996e438))

## [1.2.0](https://gitlab.com/to-be-continuous/ansible/compare/1.1.0...1.2.0) (2021-06-10)

### Features

* move group ([4bc4b7c](https://gitlab.com/to-be-continuous/ansible/commit/4bc4b7cd0b1723745dce5111468a5ec7cbdd2c8c))

## [1.1.0](https://gitlab.com/Orange-OpenSource/tbc/ansible/compare/1.0.0...1.1.0) (2021-05-18)

### Features

* add scoped variables support ([a6b69da](https://gitlab.com/Orange-OpenSource/tbc/ansible/commit/a6b69dad5f5cba5c171f21c29720f93fdca3db0a))

## 1.0.0 (2021-05-07)

### Features

* initial release ([4582745](https://gitlab.com/Orange-OpenSource/tbc/ansible/commit/4582745da1bec553c978234f15781aa66c65f9e6))
